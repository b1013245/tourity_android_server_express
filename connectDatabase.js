
/**
 * @author Asami Hasegawa
 * データベースへの接続と問い合わせを行う
 * ライブラリ(node-mysql)を使用
 */

var mysql = require('mysql');
var settings = require('./settings.js');

exports.connection = mysql.createConnection({
	host : settings.db_host,
	database : settings.database,
	user : settings.db_user,
	password : settings.db_pass,
});



/**
 * 投稿機能
 * @param connection mysqlへのコネクション
 * @param post DBへ追加する連想配列(キーはカラム名と同じ)
 * @param callback コールバック
 * 投稿機能
 * DBへ投稿を追加する
 */
exports.insert = function(connection, post, callback){
	
	connection.query('INSERT INTO ?? SET ?', [settings.table_posts, post], function(error, rows, fields){
		//console.log(rows);
		callback(error);
	});
};


/**
 * 閲覧機能
 * @param connection DBへのコネクション
 * @param latitude 緯度
 * @param longitude 経度
 * DBからデータを取得する
 * 半径1km以内かつサーバの現在時刻-5分(-5/1440)の投稿時刻のデータを問い合わせる
 * latitude(緯度)1度の距離は約111kmなので，1kmは0.009009009度
 * longitude(経度)1度の距離はcos(緯度*0.017453293)*111.3194908なので，1kmは1/Math.cos(latitude*0.017453293)*111.3194908
 * 
 */
exports.select = function(connection, latitude, longitude, callback){
	var result = [];
	var distance = 1/Math.cos(latitude*0.017453293)*111.3194908; // 1kmあたりの経度
       
	var sql = 'SELECT * FROM ?? WHERE (latitude BETWEEN ?-0.009009009 AND ?+0.009009009) AND (longitude BETWEEN ?-' + 
	distance + ' AND ?+' + distance + ') AND (uptime >= NOW() - INTERVAL 5 MINUTE);';
	connection.query(sql, [settings.table_posts, latitude, latitude, longitude, longitude], function(error, results, field){
		if(error){
			callback(error, null);
		}else{
			callback(null, results);
		}
	});
};



/**
 *  DBのデータを更新する
 * */
