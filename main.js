/**
 * @author Asami Hasegawa
 * Androidクライアントから投げられたリクエストに応じるプログラム
 * 閲覧画面と投稿画面でのやりとり 
 */
var express = require('express');
var app = express();

var fs = require('fs');
var multer = require('multer');
//ファイルの保存先
var storage = multer.diskStorage({
	destination:function(req, file, cb){
		cb(null, './uploads/');
	}
});
app.use(multer({
	dest:'./uploads/'
}));

app.use(express.bodyParser());

var db = require('./connectDatabase.js');
var http = require('http');
var server = http.createServer(app);
var settings = require('./settings.js');

/**
 * ファイル名とディレクトリを変更する
 * @param tmp_path ファイルの一時的な保存先のパス
 * @param image_path ファイルの正式な保存先のパス
 */
function changeFileDir(tmp_path, image_path){
	fs.renameSync(tmp_path, image_path); // ファイル名とディレクトリを変更
}

/**
 * 閲覧機能
 * DBにある投稿データをJSONにしてレスポンスを投げる
 * クエリを受ける
 */
app.get('/getMessages', function(req, res){
	var connection = db.connection;
	db.select(connection, Number(req.query.latitude), Number(req.query.longitude), function(error, results){
		if(error) {
			console.log('ERROR:' + error);
		}
		else{
			res.contentType('application/json');
			res.send(results);
		}
	});	
});

/**
 * 閲覧機能(サンプル)
 * 常に同じJSONを返す
 */
app.get('/getMessageSample', function(req, res){
	if(req.query.latitude){ // latitudeパラメータがあれば
		
	}
	
	var json = [
	            {"post_id":1,"user_id":1,"rated_count":100,"image_path":"uploads/hakodatePost.JPG","movie_path":null,"latitude":36,"longitude":136.5,"uptime":"2015-10-06 18:18:18"},
	            {"post_id":2,"user_id":2,"rated_count":0,"image_path":"uploads/hakodatePost.JPG","movie_path":null,"latitude":37,"longitude":134.5,"uptime":"2015-10-07 18:20:20"},
	            {"post_id":3,"user_id":3,"rated_count":100,"image_path":"uploads/hakodatePost.JPG","movie_path":null,"latitude":32,"longitude":137.5,"uptime":"2015-11-06 18:21:18"}
	            ];
	
	res.contentType('application/json');
	res.send(JSON.stringify(json));
});

/**
 * 閲覧機能
 * レスポンスに画像を返す
 * @property URLの* "uploads/画像のファイル名"
 */
app.get('/getImage/*', function(req, res){
	//var buf = fs.readFileSync('./uploads/hakodatePost.JPG');
	var image_path = req.originalUrl.slice(10, req.originalUrl.length); // "/getImage/"を除去する
	var buf = fs.readFileSync(image_path);
	res.contentType('image/JPG');
	res.send(buf);
});


/**
 * 投稿画面で投げられたパラメータを取得する
 */
app.post('/postMessage', function(req, res){
	//console.log(req.files);	
	/* 必須 */
	var user_id = req.body.user_id;
	var latitude = req.body.latitude;
	var longitude = req.body.longitude;
	var post = {user_id:user_id, latitude:latitude, longitude:longitude}; // 投稿された情報を格納する

	
	if(req.body.message){ // 文章が投稿されていたら
		post['message'] = req.body.message;
	}
	if(req.files.image_path){ // 画像が投稿されていたら
		var tmp_path = req.files.image_path.path;
		//ファイル名が重複すると，上書きされるので一時的パスを名前にする
		var target_path = req.files.image_path.path;
		var image_path = './uploads/' + target_path.substr(5); // '/tmp/'を取り除く
		changeFileDir(tmp_path, image_path);
		post['image_path'] = tmp_path;
	}
	console.log('取得データ');
	for(var item in post){
		console.log(item + ':' + post[item]);	
	}

	var connection = db.connection;
	db.insert(connection, post, function(error){
		if(error){ // INSERT失敗
			console.log(error);
			res.send('0');
		}else{ // INSERT成功
			console.log("insert success");
			res.send('1');			
		}
	}); 
});

server.listen(settings.port);
console.log('Server running');